<?php
$base_url = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Specializing in high value recoveries of non-ferrous metals from shipwrecks.">
        <meta name="author" content="Matt Rose http://glaikit.co.uk/">
        <link rel="icon" href="../../favicon.ico">

        <title>Submar Salvage</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href="css/submar.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar-wrapper">
            <div class="container">

                <nav class="navbar navbar-inverse navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo $base_url; ?>">Submar Salvage</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#">Home</a></li>
                                <li><a href="#about">About</a></li>
                                <li><a href="#contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

            </div>
        </div>

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active first-slide">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Submar Associates</h1>
                            <p>The Wealth of Non Ferrous Metals on the seabed is astounding!
                            <br>We know what it is &amp; where it is.</p>
                            <p><a class="btn btn-lg btn-primary" href="#contact" role="button">Join us</a></p>
                        </div>
                    </div>
                </div>
                <div class="item second-slide">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Play it close to the chest</h1>
                            <p>by investing in Non Ferrous Metals on the Seabed.</p>
                        </div>
                    </div>
                </div>
                <div class="item third-slide">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Expertise</h1>
                            <p>The data base for these cargoes has been built up over 30 years using our expertise in interpreting historic facts, establishing the accuracy of the information for evaluation.</p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div><!-- /.carousel -->


        <div class="container marketing">

            <!-- START THE FEATURETTES -->
            <div class="row featurette" id="about">
                <div class="col-md-7">
                    <h2 class="featurette-heading">1000&apos;s of tonnes of Copper, Tin, Zinc <span class="text-muted">and other Non-Ferrous Metals</span></h2>
                    <p class="lead">that have previously been un-located or too deep are now within reach. Underwater cargo recovery technologies have come a long way in recent years.</p>
                </div>
                <div class="col-md-5">
                    <img class="featurette-image img-responsive center-block" src="ingot.jpg" alt="Copper Ingots">
                </div>
            </div>

            <hr class="featurette-divider">

            <div class="row featurette">
                <div class="col-md-7 col-md-push-5">
                    <h2 class="featurette-heading">Our extensive database of shipwreck cargoes</h2>
                    <p class="lead">has been compiled from historical records gathered over 30 years and gives us the means to select the best candidates for economic recovery.</p>
                </div>
                <div class="col-md-5 col-md-pull-7">
                    <img class="featurette-image img-responsive center-block" src="cadmus.jpg" alt="Cadmus">
                </div>
            </div>


            <hr class="featurette-divider">

            <div class="row" id="contact">
                <!-- <div class="col-lg-6">
                    <img class="img-circle" src="graham.jpg" alt="Graham Wright" width="200" height="200">
                    <h2>Graham Wright</h2>
                    <h4>CEO</h4>
                    <p><i class="fa fa-phone"></i> <a href="tel:447808315764">+447808 315764</a></p>
                    <p><i class="fa fa-envelope"></i> <a href="mailto:gwright@submar.co.uk">gwright@submar.co.uk</a></p>
                    <p><a href="https://www.linkedin.com/pub/graham-wright/72/59a/182" target="_blank"><i class="fa fa-lg fa-linkedin-square"></i></a> <a href="https://www.facebook.com/submar.co.uk/" target="_blank"><i class="fa fa-lg fa-facebook-square"></i></a></p>
                </div> -->
                <div class="col-lg-12">
                    <img class="img-circle" src="george.jpg" alt="Major George Rose" width="200" height="200">
                    <h2>Major George Rose</h2>
                    <h4>Chairman</h4>
                    <p><i class="fa fa-phone"></i> <a href="tel:447950002466">+447950 002466</a></p>
                    <p><i class="fa fa-envelope"></i> <a href="mailto:george@submarsalvage.com">george@submarsalvage.com</a></p>
                    <p><a href="https://uk.linkedin.com/pub/george-rose/a5/738/b07" target="_blank"><i class="fa fa-lg fa-linkedin-square"></i></a><!-- <a href="https://www.facebook.com/submar.co.uk/" target="_blank"><i class="fa fa-lg fa-facebook-square"></i></a> --></p>
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->

            <hr class="featurette-divider">

            <footer>
                <p class="pull-right"><a href="#">Back to top</a></p>
                <p>&copy; Copyright <?php echo date('Y'); ?> Submar Salvage Ltd. All Rights Reserved. Company registration number: 06665583.</p>
                <p>6 Manor Park Business Centre, Mackenzie Way, Cheltenham, Gloucestershire, GL51 9TX</p>
            </footer>

        </div><!-- /.container -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/ie10-viewport-bug-workaround.js"></script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-65130476-1', 'auto');
        ga('send', 'pageview');

        </script>
    </body>
</html>